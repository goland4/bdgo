package main

import (
	"bd/pkg/producto"
	"bd/storage"
	"log"
)

func main() {
	storage.NewPostgresDB()

	// el pool es la conexión a la base de datos que es la instancia unica de la base de datos
	storageProducto := storage.NewPsqlProduct(storage.Pool())
	serviceProducto := producto.NewService(storageProducto)

	if err := serviceProducto.Migrate(); err != nil {
		log.Fatalf("Producto.Migrate: %v", err)
	}
}
