package storage

import "database/sql"

const (
	psqlCreateProductTable = `CREATE TABLE IF NOT EXISTS products (
    		id SERIAL NOT NULL,
    		name VARCHAR(100) NOT NULL,
    		price NUMERIC(10,2) NOT NULL DEFAULT 0.00,
    		observations TEXT,
    		created_at TIMESTAMP NOT NULL DEFAULT now(),
    		updated_at TIMESTAMP,
    		CONSTRAINT products_id_pk PRIMARY KEY (id)
    		);`
)

// esto es una estructura que tiene un campo db que es un puntero a sql.DB
type PsqlProduct struct {
	db *sql.DB
}

// esto es un constructor que retorna un puntero a PsqlProduct
func NewPsqlProduct(db *sql.DB) *PsqlProduct {
	return &PsqlProduct{db}
}

func (p *PsqlProduct) Migrate() error {

	// preparamos la consulta para crear la tabla
	stmt, err := p.db.Prepare(psqlCreateProductTable)
	if err != nil {
		return err
	}
	defer stmt.Close()

	// ejecutamos la consulta y ignoramos el resultado
	_, err = stmt.Exec()
	if err != nil {
		return err
	}
	return nil
}
