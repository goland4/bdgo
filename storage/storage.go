package storage

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"sync"
)

var (
	//le digo que db es un puntero a sql.DB que es la conexión a la base de datos
	db   *sql.DB
	once sync.Once // Variable que nos permite ejecutar una función solo una vez
)

const (
	host     = "localhost"
	port     = 5432       // Puerto por defecto de PostgreSQL
	user     = "postgres" // Asegúrate de que coincida con POSTGRES_USER
	password = "1234"     // Asegúrate de que coincida con POSTGRES_PASSWORD
	dbname   = "bdgo"     // Asegúrate de que coincida con POSTGRES_DB
)

func NewPostgresDB() {

	//esto es para que la función solo se ejecute una vez
	once.Do(func() {
		var err error
		conStr := fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable", user, password, host, port, dbname)

		db, err = sql.Open("postgres", conStr)
		if err != nil {
			log.Fatalf("can't open db: %v", err)
		}
		//el db.Ping() es para verificar que la conexión a la base de datos sea correcta
		if err = db.Ping(); err != nil {
			log.Fatalf("can't do ping: %v", err)
		}

		fmt.Println("Conexión a la base de datos exitosa")
	})
}

// esta función nos permite obtener la conexión a la base de datos
func Pool() *sql.DB {
	// retornamos la conexión a la base de datos
	return db
}
