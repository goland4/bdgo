package producto

import (
	"fmt"
	"time"
)

// Modelo de la tabla producto
type Model struct {
	ID           int
	Name         string
	Price        float64
	Observations string
	CreatedAt    time.Time
	UpdatedAt    time.Time
}

func (m *Model) String() string {
	return fmt.Sprintf("%02d | %-20s | %-20s | %5d | %10s | %10s",
		m.ID, m.Name, m.Observations, m.Price,
		m.CreatedAt.Format("2006-01-02"), m.UpdatedAt.Format("2006-01-02"))
}

// Esto es un slice de Model
type Models []*Model

type Storage interface {
	Migrate() error
	//Create(*Model) error
	//Update(*Model) error
	//GetAll() (Models, error)
	//GetByID(uint) (*Model, error)
	//Delete(uint) error
}

// Service es la estructura del servicio
type Service struct {
	storage Storage
}

// NewService es el constructor del servicio
func NewService(s Storage) *Service {
	return &Service{s}
}

// Migrate crea la tabla de productos
func (s *Service) Migrate() error {
	return s.storage.Migrate()
}
