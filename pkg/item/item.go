package item

import "time"

type Model struct {
	ID        uint
	InvoiceID uint
	ProductID uint
	CreatedAt time.Time
	UpdateAt  time.Time
}
