package invoice

import "time"

// Mdodelo de la tabla invoice
type Model struct {
	ID        int
	Client    string
	CreatedAt time.Time
	UpdateAt  time.Time
}
